## steps to reproduce

```bash
npm ci

npm run build
```

then you'll see this error message from tsc:

```
src/app.service.ts:10:12 - error TS2589: Type instantiation is excessively deep and possibly infinite.

10     return this.configService.get('foo', { infer: true });
              ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Found 1 error(s).
```

Read the file [`src/configuration.ts`](./src/configuration.ts)

