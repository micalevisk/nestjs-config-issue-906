export interface IConfig {
  foo: string
  // The following line introduces the TS error:
  bar: any
}

export default (): IConfig => ({
  foo: 'bar',
  bar: 123,
});

