import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import type { IConfig} from './configuration';

@Injectable()
export class AppService {
  constructor(private readonly configService: ConfigService<IConfig, true>) {}

  getFoo(): string {
    return this.configService.get('foo', { infer: true });
  }
}
